class Board
  attr_accessor :grid, :X, :O

  def initialize(grid = Array.new(3) {Array.new(3)})
    @grid = grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    raise "error, position is not empty" if !empty?(pos)
    self[pos] = mark
  end

  def valid_move?(move)
    empty_positions.include?(move) && move.all? {|coord| coord.between?(0,2)}
  end

  def empty?(pos)
    self[pos] == nil
  end

  def winner
    [:X, :O].each {|mark| return mark if win?(mark)}
    nil
  end

  def over?
    winner || @grid.flatten.count(nil) == 0
  end

  def win?(mark)
     lines.any? {|line| line.all? {|pos| self[pos] == mark}}
  end

  def render
    puts @grid.map {|row| row.map {|el| el.nil? ? "*" : el}.join(" ")}.join("\n")
  end


  def lines
    h_lines = (0..2).map {|row| [[row,0],[row,1],[row,2]]}
    v_lines = (0..2).map {|col| [[0, col],[1, col],[2, col]]}
    d_lines = [[[0,0],[1,1],[2,2]],[[0,2],[1,1],[2,0]]]
    h_lines + v_lines + d_lines
  end

  def positions
    p = []
    3.times do |row|
      3.times {|col| p << [row, col]}
    end
    p
  end

  def empty_positions
    positions.select {|pos| self.empty?(pos)}
  end

  def would_win?(pos, mark)
    place_mark(pos, mark)
    would_win = winner == mark
    self[pos] = nil
    would_win
  end

  if __FILE__ == $PROGRAM_NAME
    b = Board.new
    b.render
    b.place_mark([0,0], :O)
    b.place_mark([0,2], :X)
    b.place_mark([2,2], :O)
    b.render

    b.empty_positions
  end

end
