class ComputerPlayer
  attr_accessor :board, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    @board.empty_positions.each do |pos|
      return pos if  @board.would_win?(pos, mark)
    end
    @board.empty_positions.sample
  end

end

# if $PROGRAM_NAME == __FILE__
#   CP = ComputerPlayer.new("sara")
#   CP.display(@board)
#   CP.get_move
# end
