class HumanPlayer
  attr_reader :name
  attr_accessor  :mark, :board

  def initialize(name)
    @name = name
    @mark = ""

  end

  def display(board)
    @board = board
    board.render
  end

  def get_move
    puts "where is your next move? (ex. 'row, col')"
    pos = gets.chomp.split(",").map(&:to_i)

    until valid?(pos)
      puts "please enter a valid move (ex. 'row, col')"
      pos = gets.chomp.split(",").map(&:to_i)
    end
    pos
  end

  def valid?(move)

     move.length == 2
    #  && @board.valid_move?(move)
  end

end
